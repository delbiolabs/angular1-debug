# Angular 1.x Debugger

Implement simple debug feature for angular 1

for more advance system refer:

- [Chrome Addon AngularJS Graph]
- [lucalanca/angular-architecture-graph]
- [lucalanca/grunt-angular-architecture-graph]

# Install

open [greasemonkey file]:
```
https://bitbucket.org/delbiolabs/angular1-debug/raw/master/greasemonkey/angular_debugger.user.js
```

# Usage

when angular app is started write next line into console

## Module dependencies and components:

```js
console.log(window.ng_debug.createModuleHierarchy({},'eden.incoming.bol'));
```

## Router Listeners:

NgRouter:

```js
window.ng_debug.debugNgRouter(document.getElementsByTagName('body')[0]);
```

UiRouter:

```js
window.ng_debug.debugUiRouter(document.getElementsByTagName('body')[0]);
```

[greasemonkey file]: https://bitbucket.org/delbiolabs/angular1-debug/raw/master/greasemonkey/angular_debugger.user.js
[lucalanca/grunt-angular-architecture-graph]:https://github.com/lucalanca/grunt-angular-architecture-graph
[lucalanca/angular-architecture-graph]:https://github.com/lucalanca/angular-architecture-graph
[Chrome Addon AngularJS Graph]:https://chrome.google.com/webstore/detail/angularjs-graph/gghbihjmlhobaiedlbhcaellinkmlogj
