// ==UserScript==
// @name        Angular 1.x Debugger
// @namespace   angular.1x.debugger
// @description implement simple debug feature for angular 1
// @include     *
// @require     https://bitbucket.org/delbiolabs/angular1-debug/raw/master/src/angular-debugger.js
// @version     1
// @grant       none
// ==/UserScript==
var fileref=document.createElement('script');
fileref.setAttribute("type","text/javascript");
fileref.setAttribute("src", "https://bitbucket.org/delbiolabs/angular1-debug/raw/master/src/angular-debugger.js");
