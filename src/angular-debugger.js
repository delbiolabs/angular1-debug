(function(window, document, angular) {
  'use strict';

  /**
   * Inspired by:
   * - http://stackoverflow.com/a/24711132
   * - http://stackoverflow.com/a/27533937
   */

  function Debugger(){
    var _ = this;
  }

  Debugger.prototype.createModuleHierarchy = function (h,mod)
  {
    var _ = this;
    h[mod] = {};

    h[mod].s = _.createModuleServiceHierarchy(mod);

    if (angular.module(mod).requires.length === 0){
      return h;
    }
    var dependencies = {};
    angular.forEach(angular.module(mod).requires,
      function(m) {
          dependencies = _.createModuleHierarchy(dependencies, m);
      }
    );
    h[mod].d = dependencies;
    return h;
  };

  Debugger.prototype.createModuleServiceHierarchy = function (mod) {
    var h = {};
    angular.module(mod)['_invokeQueue'].forEach(function(value){
      if (typeof h[value[1]] === 'undefined' )
        h[value[1]] = [];
      h[value[1]].push(value[2][0]);
    });
    return h;
  };

  Debugger.prototype.debugNgRouter = function (domRootAppElem) {
    // ref: https://docs.angularjs.org/api/ngRoute/service/$route

    var _ = this;
    // document.getElementsByTagName('body')[0]
    var $rootScope = _.getComponentFromDI(domRootAppElem, '$rootScope');

    $rootScope.$on('$routeChangeStart',function(angularEvent, next, current){
      console.log('$routeChangeStart - fired when the transition begins. : ',
        '\nangularEvent:',angularEvent,
        '\nnext: ', next,
        '\ncurrent: ', current
      );
    });
    $rootScope.$on('$routeChangeSuccess',function(angularEvent, current, previous){
      console.log('$routeChangeSuccess - fired after a route change has happened successfully. : ',
        '\nangularEvent:',angularEvent,
        '\nprevious:', previous,
        '\ncurrent: ', current
      );
    });
    $rootScope.$on('$routeChangeError',function(angularEvent, current, previous, rejection){
      console.log('$routeChangeError - fired  if a redirection function fails or any redirection or resolve promises are rejected. : ',
        '\nangularEvent:',angularEvent,
        '\nprevious:', previous,
        '\ncurrent: ', current,
        '\nrejection:', rejection
      );
    });
    $rootScope.$on('$routeUpdate',function(angularEvent, current){
      console.log('$routeUpdate - The reloadOnSearch property has been set to false, and we are reusing the same instance of the Controller. : ',
        '\nangularEvent:',angularEvent,
        '\ncurrent: ', current
      );
    });
  };

  Debugger.prototype.debugUiRouter = function (domRootAppElem) {
    // ref: http://www.henry-chong.com/debugging-the-angular-js-routing-lifecycle

    var _ = this;
    // document.getElementsByTagName('body')[0]
    var $rootScope = _.getComponentFromDI(domRootAppElem, '$rootScope');

    $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
      console.log('$stateChangeStart to '+toState.to+'- fired when the transition begins. toState,toParams : \n',toState, toParams);
    });
    $rootScope.$on('$stateChangeError',function(event, toState, toParams, fromState, fromParams){
      console.log('$stateChangeError - fired when an error occurs during transition.');
      console.log(arguments);
    });
    $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
      console.log('$stateChangeSuccess to '+toState.name+'- fired once the state transition is complete.');
    });
    // $rootScope.$on('$viewContentLoading',function(event, viewConfig){
    //   // runs on individual scopes, so putting it in "run" doesn't work.
    //   console.log('$viewContentLoading - view begins loading - dom not rendered',viewConfig);
    // });
    $rootScope.$on('$viewContentLoaded',function(event){
      console.log('$viewContentLoaded - fired after dom rendered',event);
    });
    $rootScope.$on('$stateNotFound',function(event, unfoundState, fromState, fromParams){
      console.log('$stateNotFound '+unfoundState.to+'  - fired when a state cannot be found by its name.');
      console.log(unfoundState, fromState, fromParams);
    });
  };

  Debugger.prototype.getComponentFromDI = function (domRootAppElem, componentName) {
    return angular.element(domRootAppElem).injector().get(componentName);
  };

  // Singleton Window Global variable
	if (typeof window.ng_debug == "undefined") {
	    window.ng_debug = new Debugger();
  }

})(window, document, angular);
